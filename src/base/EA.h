#ifndef FSSP_EA_H
#define FSSP_EA_H

#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

#include <eval/smartEval.h>
#include "base/solution.h"
#include "base/sample.h"

class Population : public std::vector<Solution<double>> {
public:
    Population() : std::vector<Solution<double>>() {

    }

    Population(const Population &_pop) : std::vector<Solution<double>>(_pop) {

    }

    Population(const unsigned &mu) : std::vector<Solution<double>>(mu) {

    }

    struct Cmp
    {
        bool operator()(const Solution<double> & a, const Solution<double> & b) const
        {
            return b.fitness() < a.fitness();
        }
    };

    void sort() {
        std::sort(begin(), end(), Cmp());
    }

    Solution<double> & bestSolution() {
        Solution<double> * best = & ((*this)[0]);
        for(int i = 1; i < this->size(); i++)
            if (best->fitness() < (*this)[i].fitness())
                best = & ((*this)[i]);

        return *best;
    }

};

class EA {
public:

    EA(std::string const &file, unsigned _mu, unsigned _lambda, unsigned _nbGenMax,
            double _pXover, double _pMutation):
    _file("../instances/"+file), mu(_mu), lambda(_lambda),
    nbGenMax(_nbGenMax), pXover(_pXover), pMutation(_pMutation)
            {
                std::vector<std::string> getFile;
                std::istringstream input(file);
                std::string line;
                while(std::getline(input, line, '_')){
                    getFile.push_back(std::move(line));
                }

                auto toStr = getFile;
                _nbStates = std::stoi(toStr[1]);
                _graine = std::stoi(toStr[2]);
                _sol = Solution<double>(_nbStates, 2);
                std::mt19937 gen(_graine);
            }

    void initialisation(Population & parents, SmartEval seval){
        for(unsigned i=0; i<parents.size(); i++) {
            for (auto & i : _sol.function) {
                for (auto & j : i) {
                    j = gen() % (_nbStates-1);
                }
            }
            parents[i]=_sol;
        }
    }

    static void evalPop(Population & pop, SmartEval seval){
        for(unsigned i=0; i<pop.size(); i++) {
            seval(pop[i]);
        }
    }

    void selection(Population &parents, Population &children){
        for(unsigned i=0; i<lambda; i++){
            int i1 = rnd(mu);
            int i2 = rnd(mu);
            if(parents[i1].fitness() < parents[i2].fitness()){
                children[i]=parents[i2];
            }
            else{
                children[i]= parents[i1];
            }
        }
    }

    void variation(Population &children){
        for(unsigned i=0; i<children.size(); i+=2){
            if(rndD() < pXover){
                croisementIndiv(children[i], children[i+1]);
            }
        }
        for(unsigned i=0; i<children.size(); i++){
            if(rndD() < pMutation){
                mutationIndiv(children[i]);
            }
        }
    }

    void mutationIndiv(Solution<double> &sol){
        for(unsigned i=0; i<sol.nStates; i++){
            for(unsigned j=0; j<sol.alphabetSize; j++) {
                if (rndD() < 1.0 / sol.nStates) {
                    sol.function[i][j] = !sol.function[i][j];
                }
            }
        }
    }

    void croisementIndiv(Solution<double> &p1, Solution<double> &p2){
        for (int i = 0; i < _sol.nStates; i++) {
            for (int j = 0; j < _sol.alphabetSize; j++) {
                if(rndD() < 0.5) {
                    bool tmp = p1.function[i][j];
                    p1.function[i][j] = p2.function[i][j];
                    p2.function[i][j] = tmp;
                }
            }
        }
    }

    bool remplacement(Population &parents, Population &children){
        parents.sort();
        children.sort();
        bool retour = false;

        int i1=-1, i2=-1;
        for(unsigned i = 0; i < mu; i++){
            if(parents[i1+1].fitness() < children[i2+1].fitness()){
                i2++; // ne fonctionne pas avec lambda < mu !!!!!
            }
            else{
                i1++;
            }
        }
        if(i2>-1) {
            for (unsigned j = 0; j < i2; j++) {
                parents[i1 + 1 + j] = children[j];
                if(!retour) retour = !retour;
            }
        }
        return retour;
    }

    virtual void run(){
        Sample sample(_file.c_str());
        std::mt19937 gen(_graine);
        SmartEval seval(gen, sample);

        Population parents(mu);
        Population children(lambda);

        initialisation(parents,seval);
        evalPop(parents,seval);

        int cpt= 0;

        for(unsigned g=0; g < nbGenMax; g++){
            cpt++;
            selection(parents,children);
            variation(children);
            evalPop(children,seval);
            if(remplacement(parents, children)) cpt=0;
            if(cpt>= nbGenMax/2) break;
        }
        _sol = parents.bestSolution();
        //std::cout<<std::endl<< "A candidate solution with Algorithm Evolutionnary :";
        //std::cout<<std::endl<<_sol<<std::endl;
    }

    int rnd(unsigned int i) {
        return gen()%i;
    }

    double rndD() {
        std::uniform_real_distribution<double> dist{0.0, 1.0};
        return dist(gen);
    }

    double getFitness(){
        return _sol.fitness();
    }


private:

    unsigned mu;
    unsigned lambda;
    unsigned nbGenMax;
    double pXover;
    double pMutation;
    std::mt19937 gen;

    std::string _file;
    int _nbStates;
    int _graine;
    Solution<double> _sol;
};

#endif //FSSP_EA_H

