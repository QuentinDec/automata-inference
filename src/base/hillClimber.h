//Modifier le fichier demo.cpp
//Pour augmenter le nombre d'essai
//Ou changer le fichier de la graine et des états
#ifndef FSSP_HILLCLIMBER_H
#define FSSP_HILLCLIMBER_H

#include <string>
#include <vector>
#include <sstream>
#include <utility>

#include <eval/smartEval.h>
#include "base/solution.h"
#include "base/sample.h"


class HillClimber {
public :

    HillClimber(std::string const &file, unsigned nbTest):
    _file("../instances/"+file), _nbTest(nbTest)
    {
        std::vector<std::string> getFile;
        std::istringstream input(file);
        std::string line;
        while(std::getline(input, line, '_')){
            getFile.push_back(std::move(line));
        }

        auto toStr = getFile;
        _nbStates = std::stoi(toStr[1]);
        _graine = std::stoi(toStr[2]);
        _sol = Solution<double>(_nbStates, 2);
    }

    double getFitness(){
        return _sol.fitness();
    }

    void run() {
        std::mt19937 gen(_graine);
        for (auto & i : _sol.function) {
            for (auto & j : i) {
                j = gen() % (_nbStates-1);
            }
        }

        Sample sample(_file.c_str());
        SmartEval seval(gen, sample);
        seval(_sol);
        Solution<double> best = _sol;

        for(int nbEval=1; nbEval < _nbTest; nbEval++){
            double bestFitness = best.fitness();
            std::vector<int> neighbors = {-1, -1, -1};
            for (int i = 0; i < _sol.nStates; i++) {
                for (int j = 0; j < _sol.alphabetSize; j++) {
                    int previous = _sol.function[i][j];
                    _sol.function[i][j] = (int)gen() % (_nbStates-1);
                    seval(_sol);
                    if (_sol.fitness() > bestFitness) {
                        bestFitness = _sol.fitness();
                        neighbors = {i,j,_sol.function[i][j]};
                    }
                    _sol.function[i][j] = previous;
                }
            }
            if (best.fitness() > bestFitness || neighbors[0]== -1) {
                break;
            }
            _sol.function[neighbors[0]][neighbors[1]] = neighbors[2];
            best = _sol;
        }

        _sol = best;
        //std::cout<<std::endl<< "A candidate solution with HillClimber :"<<std::endl<<_sol<<std::endl;
    };


private:
    std::string _file;
    int _nbStates;
    int _graine;
    unsigned _nbTest;
    Solution<double> _sol;
};

#endif //FSSP_HILLCLIMBER_H
