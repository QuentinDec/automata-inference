/*
  demo.cpp

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/09/03 : Version 0

***
    Demo of candidate solution evaluation 

*** To compile from automata-inference directory:

mkdir build
cd build
cmake ../src/exe
make


*** To run:
./demo

*/
#include <iostream>
#include <fstream>
#include <random>

#include <base/sample.h>
#include <base/solution.h>
#include <base/hillClimber.h>
#include <base/EA.h>

#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>

using namespace std;


typedef pair<double, unsigned> Fitness2;

/* 
    Main
*/
int main(int argc, char ** argv) {
    // minimal example of sample
    Sample sample("../instances/small_sample.json");

    // print the sample if you need
    cout << "sample:" << endl;
    cout << sample << endl << endl;
    
    // Candidate solution with 4 states, 2 digits, and a fitness value which is correct classification rate
    Solution<double> x(4, 2);

    cout << "Print an empty solution:" << endl;
    cout << x << endl << endl;

    // Another candidate solution, read from file
    Solution<double> xprime;

    // read from a file this solution
    fstream filein("../instances/solution.json");

    if (!filein)
        std::cerr << "Impossible to open solution.json" << std::endl;

    filein >> xprime;

    filein.close();

    // print the solutions
    
    cout << "Solution from file:" << endl;
    cout << xprime << endl << endl;

    // Evaluation : transition function, and acceptance states are taken into account
    //              fitness is the classification rate
    BasicEval eval(sample);

    eval(xprime);

    cout << "Solutions after full evaluation:" << endl;
    cout << xprime << endl << endl;


    // Evaluation : transition function is taken into account. Acceptance states are optimal ones.
    //              fitness is the classification rate

    // random generator with random seed 1
  	std::mt19937 gen( 1 ); 

  	SmartEval seval(gen, sample);

    seval(xprime);

    cout << "solutions after function evaluation:" << endl;
    cout << xprime << endl << endl;

    // Solution with fitness = (classifcaiton rate, nb of active states)
    Solution< Fitness2 > x2;

    fstream filein2("../instances/solution_biobj.json");

    if (!filein2)
        std::cerr << "Impossible to open solution_biobj.json" << std::endl;

    filein2 >> x2 ;

    filein2.close();

    /*
	BasicBiobjEval eval2(sample);

    // evaluation of the solution
    x2.invalidate();
	eval2(x2);

    cout << x2 << endl;
    */

    // Evaluation : transition function is taken into account. Acceptance states are optimal ones.
    //              fitness is the classification rate, and number of active states
  	SmartBiobjEval seval2(gen, sample);

    // evaluation of the solution
    seval2(x2);

    cout << "A candidate solution with bi-objective fitness value:" << endl;
    cout << x2 << endl;

    for(int i= 4; i<=32; i*=2) {
        double hc001=0;
        double hc005=0;
        double hc01=0;
        double EA001=0;
        double EA005=0;
        double EA01=0;
        for(int j=0; j<30; j++) {

            std::string fichier = "dfa_" + to_string(i)+"_" + to_string(j);
            std::string test001= fichier+"_0.01_test-sample.json";
            std::string test005= fichier+"_0.05_test-sample.json";
            std::string test01= fichier+"_0.1_test-sample.json";

            HillClimber hc1(test001, 1000);
            hc1.run();
            hc001+=hc1.getFitness();
            HillClimber hc2(test005, 10000);
            hc2.run();
            hc005+=hc2.getFitness();
            HillClimber hc3(test01, 10000);
            hc3.run();
            hc01+=hc3.getFitness();

            EA ea1(test001, 10, 10, 10000, 0.5, 0.5);
            ea1.run();
            EA001 += ea1.getFitness();
            EA ea2(test005, 10, 10, 10000, 0.5, 0.5);
            ea2.run();
            EA005 += ea2.getFitness();
            EA ea3(test01, 10, 10, 10000, 0.5, 0.5);
            ea3.run();
            EA01 += ea3.getFitness();
        }
        std::cout<<std::endl<<std::endl<<std::endl;
        std::cout<<"Moyenne pour H-C 0.01 : "<<i<<"      "<<hc001/30<<std::endl;
        std::cout<<"Moyenne pour H-C 0.05 : "<<i<<"      "<<hc005/30<<std::endl;
        std::cout<<"Moyenne pour H-C 0.1 : "<<i<<"      "<<hc01/30<<std::endl;
        std::cout<<"Moyenne pour EA 0.01 : "<<i<<"      "<<EA001/30<<std::endl;
        std::cout<<"Moyenne pour EA 0.05 : "<<i<<"      "<<EA005/30<<std::endl;
        std::cout<<"Moyenne pour EA 0.1 : "<<i<<"      "<<EA01/30<<std::endl;
        std::cout<<std::flush;
    }
    // OK
    return 0;
}